﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpIntro
{
    class POI
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public POI() {}
        public POI(double la, double lo, string name, string add)
        {
            Latitude = la;
            Longitude = lo;
            Name = name;
            Address = add;
        }
        public double DistanceFrom(POI p)
        {
            //double distance = Math.Pow(Latitude - p.Latitude, 2) + Math.Pow(Longitude - p.Longitude, 2);
            //return result = Math.Sqrt(distance);
            double distance = Math.Sqrt(Math.Pow(Latitude - p.Latitude, 2) + Math.Pow(Longitude - p.Longitude, 2));
            return distance;
        }
        public override string ToString() 
        {
            return "Name: " + Name + "\tAddress: " + Address; // show latitude and longitude
        }

    }
}
