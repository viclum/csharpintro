﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpIntro
{
    class Point
    {
        private int x;
        public int X
        {
            get { return x; }
            set { x = value; }
        }
        private int y;
        public int Y
        {
            get { return y; }
            set { y = value; }
        }
        public Point() { }
        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }
        public double Distance(Point point)
        {
            double distance = Math.Sqrt(Math.Pow((X - point.X), 2) + Math.Pow((Y - point.Y), 2));
            return distance;
        }
        public override string ToString()
        {
            return "X:" + X + " Y:" + Y;
        }

    }
}