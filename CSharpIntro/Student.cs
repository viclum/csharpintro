using System;

class Student
{
    //Question 1
    //Complete the missing attributes & Properties and the incomplete student class constructor
    public int Id { get; set; } // shorter way to create properties
    public string Name { get; set; }
    // attributes and properties
    private string tel;

    public string Tel
    {
        get { return tel; }
        set { tel = value; }
    }

    private DateTime dateOfBirth;

    public DateTime DateOfBirth
    {
        get { return dateOfBirth; }
        set { dateOfBirth = value; }
    }

    // constructor
    // Student(1, "John Tan", "88552211", dob);
    public Student(int i, string n, string t, DateTime dob)
    {
        Id = i;
        Name = n;
        Tel = t;
        DateOfBirth = dob;
    }

    public override string ToString()
    {
        return "Id: " + Id + " Name: " + Name + " Tel: " + Tel + " DateOfBirth: " + DateOfBirth.ToShortDateString();
    }
}