﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Numerics;
using CSharpIntro;

void InitData(List<SavingsAccount> savingsAccCollection)
{
    string[] lines = File.ReadAllLines("savings_account.csv");
    for (int i = 1; i < lines.Length; i++)
    {
        string[] data = lines[i].Split(',');
        string an = data[0];
        string ano = data[1];
        double balance = Convert.ToDouble(data[2]);
        double rate = Convert.ToDouble(data[3]);
        SavingsAccount acc = new(an, ano, balance, rate);
        savingsAccCollection.Add(acc);
    }
}

int DisplayMenu()
{    
    Console.Write("Menu\r\n[1] Display all accounts\n[2] Deposit\n[3] Withdraw\n[0] Exit\nEnter option: \n");
    int option = Convert.ToInt32(Console.ReadLine());
    return option;
}

void DisplayAll(List<SavingsAccount> sCollection)
{
    for (int i = 0; i < sCollection.Count; i++)
    {
        var acc = sCollection[i];
        Console.WriteLine(acc.ToString());
    }
}
List<SavingsAccount> savingsAccCollection = new List<SavingsAccount>();
InitData(savingsAccCollection);
int option = -1;
while (option != 0)
{
    option = DisplayMenu();
    if (option == 1)
    {
        DisplayAll(savingsAccCollection);
    }
    else if (option == 2)
    {
        
    }
    else if (option == 3)
    {

    }
}
/*
void DisplayOutput(List<Student> sList)
{
    for (int i = 0; i < sList.Count; i++)
    {
        Student s = sList[i];
        Console.WriteLine("{0, -10} {1, -15} {2, -10} {3,-10}", s.Id, s.Name, s.Tel, s.DateOfBirth.ToShortDateString());

    }
}

List<Student> studentList = new();
DateTime dob1 = new DateTime(2006, 1, 1);
Student student1 = new Student(1, "Peter", "91112222", dob1);
studentList.Add(student1);

DisplayOutput(studentList);
*/

