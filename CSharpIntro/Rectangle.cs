﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CSharpIntro
{
    class Rectangle
    {
        private double width;
        public double Width
        {
            get { return width; }
            set { width = value; }
        }
        private double height;
        public double Height
        {
            get { return height; }
            set { height = value; }
        }
        // +Rectangle()
        public Rectangle() { }

        public Rectangle(double width, double height)
        {
            Width = width;
            Height = height;
        }

        public double FindArea()
        {
            return Width * Height;
        }
        public double FindPerimeter()
        {
            return Width * 2 + Height * 2;
        }
        public override string ToString()
        {
            return "Height: " + Height + " Width: " + Width;
        }
    }
}
